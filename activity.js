
//1.)
db.fruits.aggregate([

	{$match: {supplier:"Red Farms Inc."}},
	{$count: "itemsRedFarms"}

	])

//2.)
db.fruits.aggregate([

	{$match: {price:{$gt:50}}},
	{$count: "priceGreaterThan50"}

	])

//3.)
db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:"$supplier", avgPrice:{$avg: "$price"}}},

	])

//4.)
db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:"$supplier", maxPrice:{$max: "$price"}}},

	])

//5.)
db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:"$supplier", minPrice:{$min: "$price"}}},

	])